### 系统监控

<br />
基本问题

- 内存溢出
- 磁盘分区无空间
- tomcat挂起

+++ 

#### 期待的监控

- 系统资源（CPU，内存，磁盘，网络，主机温度）
- nginx
- redis
- postgresql
- 日志监控(tail -f web_log)
- 应用级监控
- 指定进程
- ...

---

### 基本监控方法

 针对服务器是否提供服务
  
  医路通 - [hello world页面](http://t2.nt.witontek.com/eHospital2/)

  (能够应对：内存溢出，磁盘无空间导致的tomcat挂起)

+++

### 应用级监控

Spring Boot Actuator ['æktʃʊeɪtə]

![Spring Boot Actuactor](assets/actuactor.png)

---

### 监控工具探索

#### [monit](https://mmonit.com/monit/)
![monit](assets/monit.png)

+++

![what to monit](assets/whattomonit.png) 

+++

特点
- Unix系统监控工具
- 轻量级，易安装，无依赖
- 配置灵活
- 监控进程并自动重启
- 可指定监控script（任何类型的监控）
- M/Monit 手机客户端

缺点
- 数据可视化功能弱
- 没有开箱即用地集成redis,nginx等服务器

---

#### netdata
[netdata](https://github.com/firehol/netdata/wiki) @fa[github]31.6k@fa[star]
<br/>
[t17上的Demo](http://192.168.3.237:19999/#menu_system;theme=white;help=true)

特点
- 可视化功能强大
- 实时监控(默认每秒读取数据)
- 默认提供配置好的各种系统指标监控
- 监控的内容非常丰富
    - tomcat, postgresql, redis, nginx....
    - spring boot actuator
- 通知类型多(email,slack,pushover,pushbullet,telegram... )

+++

- 配置很丰富
- 数据源扩展(influxdb, kairosdb, graphite, opentsdb ...)

缺点
- 无法查询很旧的历史数据
- 文档不是很详细

---

### 其他工具

- [grafana](https://grafana.com/) @fa[github]23.7k@fa[star]
    - [demo](https://play.grafana.org)
    - The leading open source software for time series analytics
    - 基于数据源的监控（InfluxDB, Postgres, Elasticsearch），非常灵活

- [Cabot - monitor and alert](https://cabotapp.com)
    @fa[github]4k@fa[star]

- [amon](https://www.amon.cx)
    @fa[github]1.2k@fa[star]

- 其他企业级监控
  Zabbix, Nagios

